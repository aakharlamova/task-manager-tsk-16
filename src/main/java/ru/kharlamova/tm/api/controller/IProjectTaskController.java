package ru.kharlamova.tm.api.controller;

public interface IProjectTaskController {

    void showTaskByProjectId();

    void bindTaskByProject();

    void unbindTaskFromProject();

    void removeProjectById();

}
