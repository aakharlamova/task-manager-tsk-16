package ru.kharlamova.tm.exception.system;

import ru.kharlamova.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(String command) {
        super("Error! Unknown ``"+command+"`` command.");
    }

}
