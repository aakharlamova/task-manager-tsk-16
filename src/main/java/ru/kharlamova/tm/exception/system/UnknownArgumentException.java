package ru.kharlamova.tm.exception.system;

import ru.kharlamova.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Unknown argument.");
    }

}
